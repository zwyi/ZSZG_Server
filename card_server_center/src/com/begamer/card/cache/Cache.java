package com.begamer.card.cache;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.begamer.card.common.dao.hibernate.CommDao;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.json.GiftJson;
import com.begamer.card.log.MemLogger;
import com.begamer.card.model.dao.AnnounceDao;
import com.begamer.card.model.pojo.Announce;
import com.begamer.card.model.pojo.Gift;
import com.begamer.card.model.pojo.Key;

/**
 * @author LiTao 2013-12-13 上午10:34:03
 */
public class Cache
{
	private static Logger logger = MemLogger.logger;
	public static Cache instance;
	private String version;//游戏版本(只有android包才会自动更新游戏包)
	private String downloadPath;//下载路径
	
	@Autowired
	private CommDao commDao;
	@Autowired
	private AnnounceDao announceDao;
	
	private static ConcurrentHashMap<Integer, Gift> giftMap = new ConcurrentHashMap<Integer, Gift>();// 礼包map
	public static String announce = "新加入公告功能!";
	
	public static Cache getInstance()
	{
		return instance;
	}

	public void initCache()
	{
		instance = this;
		loadGift();// 加载礼包和激活码
		loadAnnounce();
		logger.info("下载路径:"+downloadPath);
		logger.info("当前版本:"+version);
	}
	
	public void loadAnnounce()
	{
		List<Announce> ans=announceDao.find();
		if(ans!=null && ans.size()>0)
		{
			announce=ans.get(0).getAnnounce();
		}
	}
	
	/**
	 * 
	 * @Title: loadGift
	 * @Description: 加载礼包和激活码
	 * @date 2014-5-20 下午08:15:53
	 */
	public void loadGift()
	{
		long time = System.currentTimeMillis();
		List<Gift> list = commDao.getGifts();
		if (list != null && list.size() > 0)
		{
			logger.info("礼包个数:" + list.size());
			for (Gift g : list)
			{
				giftMap.put(g.getId(), g);
			}
		}
		logger.info("加载礼包激活码耗时:" + (System.currentTimeMillis() - time) + "ms");
	}

	public void deleteGift(int id)
	{
		giftMap.remove(id);
	}
	
	/**
	 * @Title: playerGetOneGift
	 * @Description: 玩家领取激活码
	 * @param playerId
	 * @param serverId
	 * @param code
	 * @date 2014-5-20 下午09:35:23
	 */
	public GiftJson playerGetOneGift(int serverId,int playerId,String code)
	{
		GiftJson gj = new GiftJson();
		String now=StringUtil.getDate(System.currentTimeMillis());
		for (Gift gift:giftMap.values())
		{
			if(now.compareTo(gift.getStartDate())>=0 && now.compareTo(gift.getEndDate())<=0)
			{
				Key key=gift.getKey(code);
				if(key!=null)
				{
					if(key.getStatus()==1)
					{
						gj.result=3;// 激活码无效
						break;
					}
					// 判断玩家是否领取过该礼包
					if(gift.haveGot(serverId, playerId))
					{
						gj.result=2;// 已经领过了
						break;
					}
					gj.result=1;
					gj.setData(gift);
					key.setStatus(1);
					key.setServerId(serverId);
					key.setPlayerId(playerId);
					key.setReceiveTime(StringUtil.getDateTime(System.currentTimeMillis()));
					commDao.updateKey(key);
					int num = commDao.getKeyUseNum();
					commDao.updateGiftUseNum(gift.getId(), num);
					break;
				}
			}
		}
		return gj;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public String getDownloadPath()
	{
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath)
	{
		this.downloadPath = downloadPath;
	}

}
