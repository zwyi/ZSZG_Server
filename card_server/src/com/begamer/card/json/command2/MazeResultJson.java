package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class MazeResultJson extends ErrorJson
{
	/**迷宫掉落时：1治疗 2号角 3炸弹 4金币 5精英战斗 6普通战斗 7boss**/
	public int type;
	/**经验或金币为数量，道具为id **/
	/**解锁迷宫的个数**/
	public int i;
	/**表示迷宫进度**/
	public int state;
	/**进入迷宫时，表示今天已经进入的次数**/
	public int num;
	/**迷宫编号**/
	public int td;
	/**迷宫的id的list**id-type(0未解锁，1解锁)-expense**/
	public List<String> s;
	/**今天已经进入过的迷宫，格式：迷宫id-进度-次数-付费标识，迷宫id-进度-次数-付费次数**/
	public String maze;
	/**人物当前等级**/
	public int lv;
	/**体力标识 0为不需扣除体力，1为需扣除体力**/
	public int t;
	
	public int mId;//最近解锁的迷宫id
	
	public int cdtime;//冷却时间--请求所有迷宫列表时服务器发给客户端
	
	//public List<PackElement> pes;//如果该位置没有装备卡，则为null
	
	public int number;//血瓶数量
	public String mazeWish;//已经许愿的物品 迷宫id-碎片id&迷宫id-碎片id
	public String mazeBossDrop;//可以许愿的物品  迷宫id-碎片id&迷宫id-碎片id
	public String cb;//cardid-blood&cardid-blood
	public String mcb;//最大
	
	public int getType(){
		return type;
	}
	public void setType(int type){
		this.type = type;
	}
	public int getI(){
		return i;
	}
	public void setI(int i){
		this.i = i;
	}
	public int getState(){
		return state;
	}
	public void setState(int state){
		this.state = state;
	}
	public int getNum(){
		return num;
	}
	public void setNum(int num){
		this.num = num;
	}
	public int getTd() {
		return td;
	}
	public void setTd(int td) {
		this.td = td;
	}
	public List<String> getS() {
		return s;
	}
	public void setS(List<String> s) {
		this.s = s;
	}
	public String getMaze() {
		return maze;
	}
	public void setMaze(String maze) {
		this.maze = maze;
	}
	public int getLv() {
		return lv;
	}
	public void setLv(int lv) {
		this.lv = lv;
	}
	public int getT() {
		return t;
	}
	public void setT(int t) {
		this.t = t;
	}
	public int getMId() {
		return mId;
	}
	public void setMId(int id) {
		mId = id;
	}
	public int getCdtime() {
		return cdtime;
	}
	public void setCdtime(int cdtime) {
		this.cdtime = cdtime;
	}
	
//	public List<PackElement> getPes() {
//		return pes;
//	}
//	public void setPes(List<PackElement> pes) {
//		this.pes = pes;
//	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getMazeWish() {
		return mazeWish;
	}
	public void setMazeWish(String mazeWish) {
		this.mazeWish = mazeWish;
	}
	public String getMazeBossDrop() {
		return mazeBossDrop;
	}
	public void setMazeBossDrop(String mazeBossDrop) {
		this.mazeBossDrop = mazeBossDrop;
	}
	public String getCb()
	{
		return cb;
	}
	public void setCb(String cb)
	{
		this.cb = cb;
	}
	public String getMcb()
	{
		return mcb;
	}
	public void setMcb(String mcb)
	{
		this.mcb = mcb;
	}
	
}
