package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class MailJson extends BasicJson
{
	//==邮件索引==//
	public int i;
	//==操作:1打开,2领取附件==//
	public int t;
	
	public int getI()
	{
		return i;
	}
	public void setI(int i)
	{
		this.i = i;
	}
	public int getT()
	{
		return t;
	}
	public void setT(int t)
	{
		this.t = t;
	}
}
