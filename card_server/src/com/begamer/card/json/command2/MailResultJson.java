package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.model.pojo.Mail;

public class MailResultJson extends ErrorJson
{
	public String title;
	public String content;
	public String reward1;//格式:type&id&number
	public String reward2;
	public String reward3;
	public String reward4;
	public String reward5;
	public String reward6;
	public int gold;
	public int crystal;
	public int runeNum;
	public int honor;//荣誉点
	public int diamond;//金刚心
	public int power;
	public int friendNum;
	public int mark;//==1领取按钮,0关闭按钮==//
	
	public void setData(Mail mail)
	{
		title=mail.getTitle();
		content=mail.getContent();
		reward1=mail.getReward1();
		reward2=mail.getReward2();
		reward3=mail.getReward3();
		reward4=mail.getReward4();
		reward5=mail.getReward5();
		reward6=mail.getReward6();
		gold=mail.getGold();
		crystal=mail.getCrystal();
		runeNum=mail.getRuneNum();
		honor = mail.getHonor();
		diamond = mail.getDiamond();
		power=mail.getPower();
		friendNum=mail.getFriendNum();
		mark=mail.haveAttach()?1:0;
	}
	
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public String getReward1()
	{
		return reward1;
	}
	public void setReward1(String reward1)
	{
		this.reward1 = reward1;
	}
	public String getReward2()
	{
		return reward2;
	}
	public void setReward2(String reward2)
	{
		this.reward2 = reward2;
	}
	public String getReward3()
	{
		return reward3;
	}
	public void setReward3(String reward3)
	{
		this.reward3 = reward3;
	}
	public int getGold()
	{
		return gold;
	}
	public void setGold(int gold)
	{
		this.gold = gold;
	}
	public int getCrystal()
	{
		return crystal;
	}
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	public int getRuneNum()
	{
		return runeNum;
	}
	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}
	public int getPower()
	{
		return power;
	}
	public void setPower(int power)
	{
		this.power = power;
	}
	public int getFriendNum()
	{
		return friendNum;
	}
	public void setFriendNum(int friendNum)
	{
		this.friendNum = friendNum;
	}
	public int getMark()
	{
		return mark;
	}
	public void setMark(int mark)
	{
		this.mark = mark;
	}
	public String getReward4()
	{
		return reward4;
	}
	public void setReward4(String reward4)
	{
		this.reward4 = reward4;
	}
	public String getReward5()
	{
		return reward5;
	}
	public void setReward5(String reward5)
	{
		this.reward5 = reward5;
	}
	public String getReward6()
	{
		return reward6;
	}
	public void setReward6(String reward6)
	{
		this.reward6 = reward6;
	}

	public int getHonor() {
		return honor;
	}

	public void setHonor(int honor) {
		this.honor = honor;
	}

	public int getDiamond() {
		return diamond;
	}

	public void setDiamond(int diamond) {
		this.diamond = diamond;
	}
	
}
