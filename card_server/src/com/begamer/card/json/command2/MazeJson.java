package com.begamer.card.json.command2;

public class MazeJson {

	/*******迷宫-位置-次数-付费标识(0未付，1已付)，迷宫-位置-次数-付费标识*****/
	
	public String name;
	public String scene;
	public int num;
	public String pay;
	
	

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
	
	
	
}
