package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class BreakJson extends BasicJson {
	public int index;// 目标卡的索引
	
	public List<Integer> list;// 被突破的索引list
	
	public int getIndex()
	{
		return index;
	}
	
	public void setIndex(int index)
	{
		this.index = index;
	}
	
	public List<Integer> getList()
	{
		return list;
	}
	
	public void setList(List<Integer> list)
	{
		this.list = list;
	}
	
}
