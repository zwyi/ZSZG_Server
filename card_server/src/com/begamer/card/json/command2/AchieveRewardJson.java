package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class AchieveRewardJson extends BasicJson
{
	public int acId;//==成就Id,excel表里对应的id==//

	public int getAcId()
	{
		return acId;
	}

	public void setAcId(int acId)
	{
		this.acId = acId;
	}
	
}
