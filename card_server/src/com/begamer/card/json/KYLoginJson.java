package com.begamer.card.json;

public class KYLoginJson
{
	public int code;
	public String msg;
	public KYUserJson data;
	
	public int getCode()
	{
		return code;
	}
	public void setCode(int code)
	{
		this.code = code;
	}
	public String getMsg()
	{
		return msg;
	}
	public void setMsg(String msg)
	{
		this.msg = msg;
	}
	public KYUserJson getData()
	{
		return data;
	}
	public void setData(KYUserJson data)
	{
		this.data = data;
	}
}
