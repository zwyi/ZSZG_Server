package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class CardGetData implements PropertyReader
{
	/**编号**/
	public int id;
	/**名称**/
	public String name;
	/**1友情值,2水晶,3屌丝券**/
	public int type;
	/**活动标志:1活动**/
	public int activity;
	/**金额**/
	public int cost;
	/**抽取次数**/
	public int gettime;
	/**增加幸运值**/
	public int luckyadd;
	/**库消耗的幸运值,库**/
	public List<Integer> luckys;
	public List<Integer> cardHouses;
	
	private static HashMap<Integer, CardGetData> data=new HashMap<Integer, CardGetData>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id=StringUtil.getInt(ss[location]);
		name=StringUtil.getString(ss[location+1]);
		type=StringUtil.getInt(ss[location+2]);
		activity=StringUtil.getInt(ss[location+3]);
		cost=StringUtil.getInt(ss[location+4]);
		gettime=StringUtil.getInt(ss[location+5]);
		luckyadd=StringUtil.getInt(ss[location+6]);
		int length=(ss.length-7)/2;
		luckys=new ArrayList<Integer>();
		cardHouses=new ArrayList<Integer>();
		for(int i=0;i<length;i++)
		{
			location=7+i*2;
			if(StringUtil.getInt(ss[location+1])!=0)
			{
				luckys.add(StringUtil.getInt(ss[location]));
				cardHouses.add(StringUtil.getInt(ss[location+1]));
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static CardGetData getData(int id)
	{
		return data.get(id);
	}
	
}
