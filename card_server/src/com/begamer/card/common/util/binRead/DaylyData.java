package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.List;

public class DaylyData implements PropertyReader
{
	//==编号==//
	public int id;
	//==奖励类型==//
	public int type;
	//==物品ID==//
	public int cardID;
	//==数量==//
	public int number;
	public int viptype;
	public int viplevel;
	public int number2;
	

	private static List<DaylyData> data=new ArrayList<DaylyData>();
	
	@Override
	public void addData()
	{
		data.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static DaylyData getData(int times)
	{
		return data.get(times);
	}
	
	public static int getAllSignTimes()
	{
		return data.size();
	}
	
}
