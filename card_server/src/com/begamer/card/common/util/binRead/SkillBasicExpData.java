package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class SkillBasicExpData implements PropertyReader{
	public int star;
	public int basicexp;
	
	private static HashMap<Integer, SkillBasicExpData> data = new HashMap<Integer, SkillBasicExpData>();
	public void addData()
	{
		data.put(star, this);
	}
	@Override
	public void resetData()
	{
		data.clear();
	}
	@Override
	public void parse(String[] ss)
	{
		
	}

	public static SkillBasicExpData getData(int star)
	{
		return data.get(star);
	}
	

}
