package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BloodBuffData implements PropertyReader
{
	public int num;
	public int type;
	public int target;
	public float effect;
	public int probability;
	
	private static HashMap<Integer, BloodBuffData> data =new HashMap<Integer, BloodBuffData>();
	private static List<BloodBuffData> dataList =new ArrayList<BloodBuffData>();
	@Override
	public void addData()
	{
		data.put(num, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData() {
		data.clear();
	}
	
	public static List<BloodBuffData> getDatas()
	{
		return dataList;
	}
	
	public static BloodBuffData getBloodBuffData(int index)
	{
		return data.get(index);
	}
	
	public static List<BloodBuffData> getBloodBuffDatas(int t)
	{
		List<BloodBuffData> list =new ArrayList<BloodBuffData>();
		for(BloodBuffData bd:data.values())
		{
			if(bd.type==t)
			{
				list.add(bd);
			}
		}
		return list;
	}
}
