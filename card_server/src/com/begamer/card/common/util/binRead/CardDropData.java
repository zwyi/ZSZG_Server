package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CardDropData implements PropertyReader 
{
	public int id;
	public int droptypeID;
	public int type;
	public int cardID;
	public int probability;
	
	private static HashMap<Integer, CardDropData> data =new HashMap<Integer, CardDropData>();
	@Override
	public void addData() {
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) {

	}

	@Override
	public void resetData() {
		data.clear();
	}
	
	public static List<CardDropData> getRandCardDropDatas(int droptypeid)
	{
		List<CardDropData> list =new ArrayList<CardDropData>();
		for(CardDropData cData: data.values())
		{
			if(cData.droptypeID ==droptypeid)
			{
				list.add(cData);
			}
		}
		return list;
	}

}
