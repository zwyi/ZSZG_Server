package com.begamer.card.common.util.binRead;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RechargeData implements PropertyReader {

	public int id;
	public int type;//类型
	public String name;//名称
	public String description;//描述
	public int crystal;//获得水晶
	public String icon;
	public int cost;
	public int double1;
	
	private static HashMap<Integer, RechargeData> data =new HashMap<Integer, RechargeData>();
	private static List<RechargeData> dataList =new ArrayList<RechargeData>();
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss) {

	}
	@Override
	public void resetData() {
		dataList.clear();
		data.clear();
	}
	
	public static RechargeData getRechargeData(int index)
	{
		return data.get(index);
	}
	
	/**获取首冲奖励的id**/
	public static List<String> getIds()
	{
		List<String> ids =new ArrayList<String>();
		for(RechargeData rData :dataList)
		{
			if(rData.double1==1)
			{
				ids.add(rData.id+"");
			}
		}
		
		return ids;
	}
	
	public static List<RechargeData> getDataList()
	{
		return dataList;
	}
	
}
